const mongoose = require("mongoose");

const userModel = mongoose.model(
  "users",
  mongoose.Schema(
    {
      userName: {
        type: String,
      },
      accountNumber: {
        type: String,
      },
      emailAddress: {
        type: String,
      },
      identityNumber: {
        type: Number,
      },
    },
    { timestamps: false }
  )
);

module.exports = userModel;
