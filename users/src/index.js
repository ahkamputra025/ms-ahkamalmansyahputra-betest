require("dotenv").config();
require("./config/")();
const express = require("express");
const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const userRoutes = require("./routes/user.route");
const generateTokenRoutes = require("./routes/generateToken.route");

app.use("/api/idstar/", userRoutes, generateTokenRoutes);

app.get("/", (req, res) => {
  res.send("This is our main endpoint");
});

app.all("*", (req, res) => {
  res.send("Are you lost...!?");
});

const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => {
  console.log(
    "Server Up and running! -- This is our Users service on port %s...",
    server.address().port
  );
});
