const express = require("express");
const router = express.Router();

const { generateJwtToken } = require("../controllers/generateJwt.controller");

router.post("/generateToken", generateJwtToken);

module.exports = router;
