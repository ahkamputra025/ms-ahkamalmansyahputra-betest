const jwt = require("jsonwebtoken");
const method = {};

method.checkToken = (req, res, next) => {
  const jwtToken = req.header("Authorization");
  if (!jwtToken)
    return res.status(401).json({ message: "Failed to authenticate token." });

  try {
    const verified = jwt.verify(jwtToken, process.env.SECRET_KEY);
    req.jwtToken = verified;
    next();
  } catch (error) {
    res.status(400).json({ message: "Token not verified." });
  }
};

module.exports = method;
