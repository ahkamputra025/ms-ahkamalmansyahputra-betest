# Documentation

Tech test Id Star

## Getting started

1. Move yourself to the backend folder: `cd app`
2. Copy the .env.Example file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
3. Install node-modules `$ npm i` and run with command `$ npm start`

# API List

service ecommerce API

| Routes | EndPoint                                                                          | Description                                  |
| ------ | --------------------------------------------------------------------------------- | -------------------------------------------- |
| POST   | https://app-idstar.herokuapp.com/api/idstar/createUser                            | Create user and get token for authentication |
| GET    | https://app-idstar.herokuapp.com/api/idstar/readUser                              | Read all user                                |
| PUT    | https://app-idstar.herokuapp.com/api/idstar/updateUser/{id}                       | Update user                                  |
| DELETE | https://app-idstar.herokuapp.com/api/idstar/deleteUser/{id}                       | Delete user                                  |
| GET    | https://app-idstar.herokuapp.com/api/idstar/readByAccountNumber/{accountNumber}   | Read by account Number                       |
| GET    | https://app-idstar.herokuapp.com/api/idstar/readByIdentityNumber/{identityNumber} | Read by indenity Number                      |
| POST   | https://app-idstar.herokuapp.com/api/idstar/generateToken                         | Generate token                               |

Postment documentation
https://documenter.getpostman.com/view/17902429/UVC3k8TZ
