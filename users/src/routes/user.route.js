const express = require("express");
const router = express.Router();

const {
  createUser,
  readUser,
  updateUser,
  deleteUser,
  readByAccountNumber,
  readByIdentityNumber,
} = require("../controllers/user.controller");
const { checkToken } = require("../middlaware/authJwt.middleware");

router.post("/createUser", createUser);
router.get("/readUser", checkToken, readUser);
router.put("/updateUser/:id", checkToken, updateUser);
router.delete("/deleteUser/:id", checkToken, deleteUser);

router.get("/readByAccountNumber/:an", checkToken, readByAccountNumber);
router.get("/readByIdentityNumber/:identNum", checkToken, readByIdentityNumber);

module.exports = router;
