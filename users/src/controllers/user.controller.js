const User = require("../database/model/user.model");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const method = {};

method.createUser = async (req, res) => {
  const { emailAddress } = req.body;
  try {
    const checkEmail = await User.findOne({ emailAddress });
    if (!checkEmail) {
      const dtUser = new User({
        ...req.body,
      });
      await dtUser.save();

      const jwtToken = jwt.sign(
        {
          id: dtUser.id,
          email: dtUser.emailAddress,
        },
        process.env.SECRET_KEY,
        {
          expiresIn: "30d",
        }
      );

      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Create user success.",
        result: dtUser,
        token: jwtToken,
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "Email already registered.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

method.readUser = async (req, res) => {
  try {
    const readAllUser = await User.find();

    res.status(200).json({
      statusCode: 200,
      statusText: "success",
      message: "Read all user success.",
      result: readAllUser,
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
};

method.updateUser = async (req, res) => {
  const { id } = req.params;
  const { emailAddress } = req.body;
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(401).json({ message: "Format id invalid!!!." });
  try {
    const checkEmail = await User.findOne({ emailAddress });
    if (!checkEmail) {
      const user = await User.findById(id);
      user.set({ ...req.body });
      await user.save();

      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Update user success.",
        result: user,
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "Email already registered.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

method.deleteUser = async (req, res) => {
  const { id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(401).json({ message: "Format id invalid!!!." });
  try {
    const checkId = await User.findById(id);
    if (checkId) {
      await User.deleteOne({ _id: id });
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "delete user success.",
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "id does not exist.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

method.readByAccountNumber = async (req, res) => {
  const { an } = req.params;
  try {
    const dtByAccNumber = await User.findOne({ accountNumber: an });
    if (dtByAccNumber) {
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Read by account number success.",
        result: dtByAccNumber,
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "Account number does not exist.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

method.readByIdentityNumber = async (req, res) => {
  const { identNum } = req.params;
  try {
    const dtByIdentNumber = await User.findOne({ identityNumber: identNum });
    if (dtByIdentNumber) {
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Read by Identity number success.",
        result: dtByIdentNumber,
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "Identity number does not exist.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

module.exports = method;
