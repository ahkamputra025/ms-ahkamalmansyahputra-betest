const User = require("../database/model/user.model");
const jwt = require("jsonwebtoken");
const method = {};

method.generateJwtToken = async (req, res) => {
  const { email } = req.body;
  try {
    const checkEmail = await User.findOne({ emailAddress: email });
    if (checkEmail) {
      const jwtToken = jwt.sign(
        {
          id: checkEmail.id,
          email: checkEmail.emailAddress,
        },
        process.env.SECRET_KEY,
        {
          expiresIn: "30d",
        }
      );
      res.status(200).json({
        statusCode: 200,
        statusText: "success",
        message: "Generate Token success.",
        token: jwtToken,
      });
    } else {
      res.status(400).send({
        statusCode: 400,
        statusText: "failed",
        message: "Email does not exist.",
      });
    }
  } catch (error) {
    res.status(500).send(error.message);
  }
};

module.exports = method;
